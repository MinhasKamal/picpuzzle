# PicPuzzle
#### Naive AI Aided Sliding Puzzle Game

It is a [plain Sliding Puzzle game](http://minhaskamal.github.io/PicPuzzle), but the interesting part is- it includes an Artificial Intelligent that can arrange the pieces for you. It also keeps record of the top 5 fastest solvers.

### Views
  <div align="center">
  <img src="https://cloud.githubusercontent.com/assets/5456665/12999222/215ad786-d177-11e5-92e6-80c573fd80a9.png" height="230" width="290" >
  <img src="https://cloud.githubusercontent.com/assets/5456665/12999223/215db3de-d177-11e5-8c31-8608916547c2.png" height="230" width="290" >
  <img src="https://cloud.githubusercontent.com/assets/5456665/12999221/20f345d0-d177-11e5-85c2-3a8cfa34b397.png" height="230" width="290" >
  </div>

### Releases
- <a href="https://github.com/MinhasKamal/PicPuzzle/raw/master/PicturePuzzle_V-5.3.jar">Version 5.3</a> - Release Date:  07 Jun 2014
- <a href="https://github.com/MinhasKamal/PicPuzzle/raw/master/PicturePuzzle_V-5.2.jar">Version 5.2</a> - Release Date: 12 May 2014
- <a href="https://github.com/MinhasKamal/PicPuzzle/raw/master/PicPuzzle_V-5.0.jar">Version 5.0</a> - Release Date: 14 Apr 2014
- <a href="https://github.com/MinhasKamal/PicPuzzle/raw/master/PicPuzzle_V-4.0.jar">Version 4.0</a> - Release Date: 01 Jan 2014
- <a href="https://github.com/MinhasKamal/PicPuzzle/raw/master/PicPuzzle_V-3.0.jar">Version 3.0</a> - Release Date: 25 Dec 2013
- <a href="https://github.com/MinhasKamal/PicPuzzle/raw/master/PicPuzzle_V-1.1.jar">Version 1.1</a> - Release Date: 03 Dec 2013

### License
<a rel="license" href="http://www.gnu.org/licenses/gpl.html"><img alt="GNU General Public License" style="border-width:0" src="http://www.gnu.org/graphics/gplv3-88x31.png" /></a><br/>PicPuzzle is licensed under a <a rel="license" href="http://www.gnu.org/licenses/gpl.html">GNU General Public License version-3</a>.
